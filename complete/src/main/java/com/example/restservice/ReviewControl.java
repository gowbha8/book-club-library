package com.example.restservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.example.model.Review;
import com.example.model.Reviews;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReviewControl {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	private final Reviews reviews = new Reviews();


	@GetMapping("/addReview")
	public void addReview(@RequestParam(value = "id", defaultValue = "1") int id, @RequestParam(value = "review", defaultValue = "bad") String review) {
		Review reviewBuilder = new Review();
		reviewBuilder.setReview(review);
		reviewBuilder.setId(id);
		reviewBuilder.setLikes(0);
		List<String> comments = new ArrayList<>();
		comments.add("");
//		reviewBuilder.setComments();
		reviews.add(reviewBuilder);
	}

	@GetMapping("/getReview")
	public String getReview(@RequestParam(value = "id", defaultValue = "1") int id) {

		return reviews.get(id);
	}

	@GetMapping("/upvote")
	public void upvote(@RequestParam(value = "id", defaultValue = "1") int id){
		reviews.updateLikes(id);
	}

	@GetMapping("/addComment")
	public void addComment(@RequestParam(value = "id", defaultValue = "1") int id, @RequestParam(value = "comment", defaultValue = "don't question reviews") String comment){
		reviews.updateComments(id, comment);
	}
}
