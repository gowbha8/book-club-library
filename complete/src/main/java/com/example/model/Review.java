package com.example.model;

import java.util.List;

public class Review {

    private int id;
    private String review;
    private List<String> comments;
    private int likes = 0;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

}
