package com.example.model;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Reviews {

    public void updateLikes(int id){
        try {
            MongoClient mongo = new MongoClient("10.8.9.120", 27017);
            DB db = mongo.getDB("test-db");
            DBCollection col = db.getCollection("reviews");


            DBObject query = BasicDBObjectBuilder.start().add("id", id).get();

            DBCursor cursor = col.find(query);
            cursor.next();
            cursor.next();
            cursor.next();
            String review = (((BasicDBObject) cursor.next()).getString("review"));
            int likeCount = Integer.parseInt((((BasicDBObject) cursor.next()).getString("likes")));

            Review reviewBuilder = new Review();
            reviewBuilder.setReview(review);
            reviewBuilder.setId(id);
            reviewBuilder.setLikes(likeCount+1);

            col.update(query, createDBObject(reviewBuilder));

        } catch (Exception e) {
        }
    }

    public void updateComments(int id, String comment){
        try {
            MongoClient mongo = new MongoClient("10.8.9.120", 27017);
            DB db = mongo.getDB("test-db");
            DBCollection col = db.getCollection("reviews");


            DBObject query = BasicDBObjectBuilder.start().add("id", id).get();


//            DBObject project = new BasicDBObject();
//            project.put("likes", 1);
//            DBCursor cursor = col.find(query, project);
//            String likeCount = ((BasicDBObject) cursor.next()).getString("likes");
//            BasicDBObject newDocument = new BasicDBObject();
//            newDocument.append("$set", new BasicDBObject().append("likes", likeCount+1));
//            col.update(query, newDocument);
//            while(cursor.hasNext()) {
//                System.out.println(cursor.next());
//            }

        } catch (Exception e) {
        }
    }

    public String get(int id){
        String result ="";

        try {
            MongoClient mongo = new MongoClient("10.8.9.120", 27017);
            DB db = mongo.getDB("test-db");
            DBCollection col = db.getCollection("reviews");
            DBObject query = BasicDBObjectBuilder.start().add("id", id).get();
            DBCursor cursor = col.find(query);
            while(cursor.hasNext()){
                    result = result + cursor.next();
            }
        } catch (Exception e) {
        }
        return result;
    }

    public void add (Review review){
        try {
            MongoClient mongo = new MongoClient("10.8.9.120", 27017);
            DB db = mongo.getDB("test-db");
            DBCollection col = db.getCollection("reviews");
            DBObject newReview = createDBObject(review);
            col.insert(newReview);

        } catch (Exception e) {
        }

    }

    private static DBObject createDBObject(Review review) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("id", review.getId());
        docBuilder.append("likes", review.getLikes());
        docBuilder.append("review", review.getReview());
        return docBuilder.get();
    }



}
